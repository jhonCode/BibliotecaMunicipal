<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'Auth\LoginController@showLogin');
//Route::get('login', 'Auth/LoginController@showLogin');

Route::get('login',array('as'=>'login',function()
    {
            return view('welcome');
     }
));

Route::get('/welcome', function () {
    return view('welcome');
});


//login
Route::post('/logear','Auth\LoginController@postLogin');
Route::get('/salir','Auth\LoginController@logout');



//Route::get('/login','Auth\LoginController@showLogin');


Route::get('/password', function () {
    return view('password');
});


Route::post('/correo','usuariosController@postEmail');



//grupo privado
Route::group(['middleware'=>'auth'],function(){


Route::get('/home', function () {
    return view('/php/inicio');
});



Route::get('/libro', ['as' => 'libro', 'uses' => 'zonaController@index2']);

Route::get('/usuario', function () {
    return view('/php/usuario');
});


Route::get('/prestamo', function () {
    return view('/php/prestamo');
});

Route::get('/zona', function () {
    return view('/php/zona');
});

Route::post('/registrarUsuario','usuariosController@create');

Route::get('/listadoUsuarios', ['as' => 'listadousuarios', 'uses' => 'usuariosController@index']);

Route::get('usuario/edit/{id}', ['as' => 'usuario/edit', 'uses'=>'usuariosController@edit']);

Route::get('usuario/destroy/{id}', ['as' => 'usuario/destroy', 'uses'=>'usuariosController@destroy']);

Route::post('usuario/update', ['as' => 'usuario/update', 'uses'=>'usuariosController@update']);

//para buscar un usuario
Route::get('/buscarUsuario','usuariosController@show');



Route::post('/registrarLibro','librosController@create');

Route::get('/listadolibros', ['as' => 'listadolibros', 'uses' => 'librosController@index']);

Route::get('libro/edit/{id}', ['as' => 'libro/edit', 'uses'=>'librosController@edit']);

Route::get('libro/destroy/{id}', ['as' => 'libro/destroy', 'uses'=>'librosController@destroy']);

Route::post('libro/update', ['as' => 'libro/update', 'uses'=>'librosController@update']);

//para buscar un libro
Route::get('/buscarLibro','librosController@show');


Route::post('/generarSolicitud','prestamoController@create');



Route::get('/listadoPrestamos', ['as' => 'listadoprestamos', 'uses' => 'prestamoController@index']);

Route::get('prestamo/edit/{id}', ['as' => 'prestamo/edit', 'uses' => 'prestamoController@edit']);

Route::get('libro/edit/{id}', ['as' => 'libro/edit', 'uses'=>'librosController@edit']);

Route::post('/registroZona','zonaController@create');

Route::get('/listadoZonas', ['as' => 'listadozonas', 'uses' => 'zonaController@index']);

});//cierra grupo