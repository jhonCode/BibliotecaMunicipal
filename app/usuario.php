<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Crypt;
use Hash;


/**
  *Clase encargada de manejar un crud para la tabla users
  *
  */
class usuario extends Model
{

 protected $table = 'users';
 protected $primaryKey='id';
    

   /**
     * crea un usuario en la bd.
     *@param trae los datos necesarios para registrar un usuario en la bd.
     */
      public static function crearUsuario($data)
        {


           if($data['username']!=null){

             $encrypted = Crypt::encrypt($data['password']);
    		DB::table('users')->insert(array(
                'documento' => $data['idPersona'] ,
                'nombre' => $data['nombre'],
                'apellido' => $data['apellido'],
                'edad' => $data['edad'],
                'direccion' => $data['direccion'],
                'email' => $data['email'],
                'telefono' => $data['telefono'],
                'rol' => $data['rol'] ,
                'username' =>$data['username'],
                'password'=>\Hash::make($data['password']) ,
                'password2'=>$encrypted,
            )); 
        }else{
          DB::table('users')->insert(array(
                'documento' => $data['idPersona'] ,
                'nombre' => $data['nombre'],
                'apellido' => $data['apellido'],
                'edad' => $data['edad'],
                'direccion' => $data['direccion'],
                'email' => $data['email'],
                'telefono' => $data['telefono'],
                'rol' => $data['rol'] ,
                'username' =>null,
                'password'=>null,
                'password2'=>null,
            )); 
         
        }

        

        }

    /**
     * Funcion que se encarga de buscar un usuario en la bd
     * @param  nombre de la persona a buscar
     * 
     */
    public static function  buscarUsuario($usuarioNombre)
    {
      $nombreUsuario=$usuarioNombre;
      if(trim($nombreUsuario)!=""){
      $usuario=DB::table('users')
      ->select('id','nombre')
      ->Where(DB::raw("CONCAT(users.nombre,' ', users.apellido)"),'LIKE' ,"%".$nombreUsuario."%")
      ->first();
        }

      
    
          
      return $usuario;
    }

   /**
     * Funcion que se encarga de modificar los datos del usuario
     *
     * @param  $request contiene la informacion para modificar un usuario
     */
   public static function updateUsuario($usuario)
   {

    $usuariobd =usuario::find($usuario->id);
    $usuariobd->documento =$usuario->reg_id;
    $usuariobd->nombre =$usuario->reg_nombre;
    $usuariobd->apellido =$usuario->reg_apellido;
    $usuariobd->edad =$usuario->reg_edad;
    $usuariobd->direccion =$usuario->reg_direccion;
    $usuariobd->email =$usuario->reg_email;
    $usuariobd->telefono =$usuario->reg_telefono;
    $usuariobd->rol =$usuario->rol;

    if($usuario->rol!="Lector"){ 

      $encrypted = Crypt::encrypt($usuario->reg_password);
      $usuariobd->username =$usuario->reg_username;
      $usuariobd->password =\Hash::make($usuario->reg_password);
      $usuariobd->password2 =$encrypted;
    }else{
      $usuariobd->username =null;
      $usuariobd->password =null;
      $usuariobd->password2 =null;


    }
    $usuariobd->save();

  }


   /**
     * Elimina un usuario de la bd
     * @param  idUsuario a eliminar
     */
  public static function destroyUsuario($idUsuario)
  {
  	$usuario = usuario::find($idUsuario);
  	$usuario->delete();
  }
   /**
     * Funcion que se encarga de buscar en la bd el correo electronico
     *@param $email a buscar en la bd de administradores
     * @return  devuelve la informacion del usuario
     */
  public static function buscarEmail($email)
  {
  $usuario=DB::table('users')
  ->select('nombre','password2')
  ->Where('email',$email)
  ->first();

  return $usuario;
  }


}
