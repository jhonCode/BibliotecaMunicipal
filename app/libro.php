<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Storage;



/**
  *Clase encargada de manejar un crud para la tabla libro
  *
  */
  class libro extends Model
  {
    protected $table = 'libro';
    protected $primaryKey='idLibro';


   /**
    * Registra un libro en la base de datos
    * @param trae los datos necesarios para crear un registro de la bd.
    * 
    */
   public static function crearLibro($data)
   {

     DB::table('libro')->insert(array(
       'isbn' => $data['isbn'] ,
       'foto' => $data['foto'],
       'titulo' => $data['titulo'],
       'resumen' => $data['resumen'],
       'categoria' => $data['categoria'],
       'ejemplares' => $data['ejemplar'],
       'disponibles' => $data['ejemplar'],
       'autor' => $data['autor'],
       'idZona' => $data['zona']           		           
     )); 

   }

      /**
       * Elimina un libro de la bd.
       * @param  idLibro a eliminar
       * 
       */
      public static function destroyLibro($idLibro)
      {


        $foto=DB::table('libro')
        ->select('foto')
        ->Where('idLibro',$idLibro)
        ->first();



        $nombrefoto=explode('/', $foto->foto);
        $fotoid=$nombrefoto[2];      
  //var_dump($nombrefoto[2]);
        \Storage::delete($fotoid);

        $libro = libro::find($idLibro);
        $libro->delete();

      }
   /**
       * Se encarga de buscar un libro en la bd.
       *
       * @param  $request trae el titulo del libro a consultar
       * @return  vista prestamo 
       */

   public static function buscarLibro($datos)
   {
     
    $idUser=$datos->user;
    $libro=DB::table('libro')
    ->select('idLibro','categoria')
    ->Where(DB::raw("titulo"),'LIKE' ,"%".$datos->libro."%")
    ->first();
     
      if($libro!=null){ 
      $persona= DB::table('users')
      ->select('edad')
      ->Where('id',$idUser)
      ->first();
  
     
      
      if($persona->edad<=$libro->categoria){
       return $libro; 
      }

      }   
     return null;
   }



  /**
       * Actualiza la informacion de un libro.
       *
       * @param  datos a actualizar  $request
       */
  public static function updateLibro($request){
  	$libro = libro::find ($request->id);
         //obtenemos el campo file definido en el formulario
   $file = $request->file('reg_foto');
   
         //obtenemos el nombre del archivo
   $nombre = $file->getClientOriginalName();
   $libro->isbn = $request->reg_isbn;
   $libro->titulo = $request->reg_titulo;
   $libro->resumen = $request->resumen;
   $libro->categoria = $request->reg_categoria;
   $libro->ejemplares = $request->reg_ejemplar;
   $libro->disponibles = $request->reg_disponible;
   $libro->autor = $request->reg_autor;
   $libro->idZona = $request->idZona;

   if($file!=null){
    $libro->foto = './storage/'.$nombre;
  }

  $libro->save();
  \Storage::disk('public')->put($nombre,  \File::get($file));
  }

  }
