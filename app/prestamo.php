<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\libro;
use DB;
use DateTime;


/**
 *
 *clase que se encarga de manejar los prestamos de libros
 *
 */
class prestamo extends Model
{

	// atributos especiales de la clase con respecto a la tabla
    protected $table="prestamo";
    protected $primaryKey='idPrestamo';



 /**
 *Funcion que se encarga de modificar el prestamo cuando va a finalizar
 *@param id del prestamo a modificar
 *
 */
 public static function editPrestamo($idPrestamo){

   $prestamo=prestamo::find($idPrestamo);
   $prestamo->devuelto=1;
   $prestamo->save();
   $libro=libro::find($prestamo->idLibro);
   $cantidad=$libro->disponibles;
   $libro->disponibles=$cantidad+1;
   $libro->save(); 

 }


/**
 *Funcion que se encarga de registrar el prestamo 
 *@param  contiene el idPersona idLibro fechaInicio Fecha fin
 *
 */
 public static function registrar($data){

       $dateInicio = new DateTime($data->dateInicio);
       $dateEntrega = new DateTime($data->dateEntrega);   

 	DB::table('prestamo')->insert(array(
       'idPersona' => $data['usuario'],
       'idLibro' => $data['libro'],
       'fechaInicio' => $dateInicio->format('Y-m-d'),
       'fechaFin' => $dateEntrega->format('Y-m-d'),
       'devuelto'=>0,
       'dias'=>0,        		           
     )); 

   $libro=libro::find($data->libro);
   $cantidad=$libro->disponibles;
   $libro->disponibles=$cantidad-1;
   $libro->save(); 

 }


}
