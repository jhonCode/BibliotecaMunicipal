<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
/**
  *Clase encargada de realizar consultas al modelo zona
  *
  */
class zona extends Model
{

     protected $table="zona";
    protected $primaryKey='idZona';
    
	 /**
	  *Funcion que se encarga de registrar la zona en la bd
	  *@param nombre con el cual se llamara la nueva zona
	  *@return  vista zona de registro
	  */  
	public static function crearZona($zona)
	{

	DB::table('zona')->insert(array(
	            'nombre' => $zona['reg_zona'] ,
	         )); 
	}

}
