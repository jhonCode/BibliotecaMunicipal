<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\libro;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use Redirect;
use DB;

 /**
  *Clase encargada de manejar un crud para la tabla libro
  *
  */
class librosController extends Controller
{
        /**
         * Realiza consulta a la bd para traer los libros.
         *
         * @return  la vista donde se podran visualizar los libros para editar y eliminar
         */
        public function index()
        {
            
             $libros=DB::table('libro')
                ->join('zona', 'zona.idZona', '=', 'libro.idZona')
                ->select('idLibro','isbn','foto','titulo','resumen','categoria','ejemplares','disponibles','zona.nombre','libro.autor')
                ->get();



            return view('/php/listadoLibros',['libros' => $libros]);
        
        }

        /**
         *  Registra un libro en la base de datos
         * @param trae los datos necesarios para crear un registro de la bd.
         * @return vista del registro de libros
         */
        public function create( Request $request)
        {
            
          //obtenemos el campo file definido en el formulario
           $file = $request->file('reg_foto');
     
           //obtenemos el nombre del archivo
           $nombre = $file->getClientOriginalName();
           $extension = $file->getClientOriginalExtension();

            
            $dataLibro= array(
                'isbn' => $request->reg_isbn,
                'foto' => './storage/'.$nombre,
                'titulo' => $request->reg_titulo,
                'resumen' => $request->reg_resumen,
                'categoria' => $request->reg_categoria,
                'ejemplar' => $request->reg_ejemplar,
                'autor' => $request->reg_autor,
                'zona' => $request->reg_zona
                                );
                                  
            libro::crearLibro($dataLibro);
        //indicamos que queremos guardar un nuevo archivo en el public
       \Storage::disk('public')->put($nombre,  \File::get($file));

      return Redirect::to('libro')->with('success','Registro Exitoso');

        }


    /**
     * Se encarga de buscar un libro en la bd.
     *
     * @param  $request trae el titulo del libro a consultar
     * @return  vista prestamo 
     */
    public function show(Request $request)
    {
       $idUser=$request->user;
       
        $libro = libro::buscarLibro($request);
        
        //return view('/php/prestamo',['libro' => $libro]);
        
        if($libro==null){
        return Redirect::to('prestamo')
               ->with('error_libro', 'Categoria no coincide ')
               ->with('success',$idUser);  
       }
       else{
        //return Redirect::to('prestamo')->with('success', $usuario->id); 
           
       return Redirect::to('prestamo')
             ->with('success',$libro->idLibro)
             ->with('usuario',$idUser);
  
  

                 
           
       }
         
 
         

    }



    /**
     * Actualiza la informacion de un libro.
     *
     * @param  datos a actualizar  $request
     * @return  vista listadolibros
     */
    public function update(Request $request)
    {
    libro::updateLibro($request);
    return redirect('/listadolibros');
    }

    /**
     * Elimina un libro de la bd.
     * @param  idLibro a eliminar
     * @return vista listado libros
     */
    public function destroy($idLibro)
    {
      libro::destroyLibro($idLibro);
      return redirect('/listadolibros');
    }


    /**
     *Consulta la informacion de un libro y la envia a la vista update.
     * @param  idLibro a modificar
     * @return vista update libro
     */

    public function edit($idLibro){

        $libros =DB::table('libro')
        ->join('zona', 'zona.idZona', '=', 'libro.idZona')
        ->select('idLibro','isbn','foto','titulo','resumen','categoria','ejemplares','autor','zona.idZona','disponibles')
        ->Where('idLibro',$idLibro)
        ->first();

         $zonas =DB::table('zona')
        ->select('idZona','nombre')
        ->get();



        return \View('/php/updatelibro')
                ->with('libro',$libros)
                ->with('zonas',$zonas);
                


    }


}
