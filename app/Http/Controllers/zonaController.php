<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\zona;
use Redirect;
use DB;


/**
  *Clase encargada de realizar consultas al modelo zona
  *
  */
class zonaController extends Controller
{
    
    /**
      *
      *Funcion que se encarga de agrupar las zonas por nombre
      *@return  vista listadozona
      */ 
	public function index(){

		$zonas=DB::table('zona')
	            ->join('libro', 'libro.idZona', '=','zona.idZona')
	            ->select(DB::raw('count(*) as total ,zona.nombre'))
	            ->groupBy('zona.nombre')
	            ->get();



	    return view('/php/listadozona',['zonas' => $zonas]);
	}


    /**
      *
      *Funcion que retorna las zonas para el checkbox
      *@return  vista listadolibro
      */ 
  public function index2(){

    $zonas=DB::table('zona')
              ->select('idZona','nombre')
              ->get();

      return view('/php/libro',['zonas' => $zonas]);
  }




    /**
      *Funcion que se encarga de crear una zona 
      *@param nombre con el cual se llamara la nueva zona
      *@return  vista zona de registro
      */ 
public  function create(Request $request){
     zona::crearZona($request);

  return Redirect::to('/zona')->with('success','Registro Exitoso');
}


 }
