<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\usuario;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use Redirect;
use DB;
use Mail;
use Swift_Transport;
use Swift_Message;
use Swift_Mailer;
use Crypt;

/**
  *Clase encargada de manejar un crud para la tabla users
  *
  */
class usuariosController extends Controller
{
    /**
     * realiza una consulta para traer todos los usuarios de la bd administrador y lector.
     *
     * @return vista donde se visualizan los datos de los usuarios.
     */
        public function index()
        {
             $usuarios=DB::table('users')
                ->select('id','documento','nombre','apellido','edad','direccion','email','telefono','rol','username','password2')
                ->get();

            foreach ($usuarios as $usuario) {
                if($usuario->username!=null)
            $usuario->password2=Crypt::decrypt($usuario->password2) ;
            }    

            return view('/php/listadoUsuarios',['usuarios' => $usuarios]);
        

        }

    /**
     * crea un usuario en la bd.
     *@param trae los datos necesarios para registrar un usuario en la bd.
     * @return redirecciona a la vista usuario indicando que el registro fue correcto.
     */
    public function create(Request $request)
    {

        $dataUsuario= array(
            'idPersona' => $request->reg_id,
            'nombre' => $request->reg_nombre,
            'apellido' => $request->reg_apellido,
            'edad' => $request->reg_edad,
            'direccion' => $request->reg_direccion,
            'email' => $request->reg_email,
            'telefono' => $request->reg_telefono,
            'rol' => $request->rol,
            'username' => $request->reg_username,
            'password' => $request->reg_password,
                            );
       
        usuario::crearUsuario($dataUsuario);
         return Redirect::to('usuario')->with('success', 'You are successfully'); 
    }


    /**
     * Funcion que se encarga de buscar un usuario en la bd
     * @param  nombre de la persona a buscar
     * @return  a la vista prestamo para continuar con el proceso.
     */
    public function show(Request $request)
    {
           
          $usuario = usuario::buscarUsuario($request->usuario);
        
       if($usuario==null){
        return Redirect::to('prestamo')->with('error', 'usuario no registrado');  
       }else{
        return Redirect::to('prestamo')->with('success', $usuario->id); 
    
       }


        //return view('/php/prestamo',['usuario' => $usuario]);
     

    }
    

    /**
     * Funcion que se encarga de modificar los datos de un usuario
     * @param  idUsuario usuario a modificar
     * @return  updateUsuario para obtener los datos y poderlos modificar
     */
    public function edit($idUsuario)
    {

        $persona =DB::table('users')
        ->select('id','documento','nombre','apellido','edad','direccion','email','telefono','rol','username','password2')
        ->where('id',$idUsuario)
        ->first();

    if($persona->username!=null)
     $persona->password2=Crypt::decrypt($persona->password2); ;



        return \View::make('/php/updateUsuario',compact('persona'));
        
    }

    /**
     * Funcion que se encarga de modificar los datos del usuario
     *
     * @param  $request contiene la informacion para modificar un usuario
     * @return vuelve a la vista donde estan todos los usuarios
     */
    public function update(Request $request)
    {
    usuario::updateUsuario($request);
        return redirect('/listadoUsuarios');      
    }

    /**
     * Elimina un usuario de la bd
     * @param  idUsuario a eliminar
     * @return  vuelve a la vista donde estan todos los usuarios
     */
    public function destroy($idUsuario)
    {
        usuario::destroyUsuario($idUsuario);
        return redirect('/listadoUsuarios');
    }


    
    /**
     * Funcion que se encarga de enviar un correo electronico al admin si se le olvido la clave
     * @return  vuelve a la vista donde  se realizo la peticiòn
     */

    public function postEmail(){
        $email=Input::get('fp_email');
        $mensaje="contraseña:";
        $asunto="sistema bibliotecario";
        $cadena=usuario::buscarEmail($email);
        $digest= Crypt::decrypt($cadena->password2);

        $data_toview = array();
        $data_toview['name'] = $mensaje.$digest;

        $email_sender   = "92ramirescortes@gmail.com";
        $email_pass     = 'llanitos3628';
        $email_to    = $email;

        $backup = \Mail::getSwiftMailer();

        try{

            $transport = \Swift_SmtpTransport::newInstance('smtp.gmail.com', 587, 'tls');
            $transport->setUsername($email_sender);
            $transport->setPassword($email_pass);

            $gmail = new Swift_Mailer($transport);

            \Mail::setSwiftMailer($gmail);

            $data['emailto'] =$email_to;  
            $data['sender'] = $email_sender;
            $data['asunto']=$asunto;

            Mail::send('emails.html', $data_toview, function($message) use ($data)
            {

                $message->from($data['sender'], 'Administrador');
                $message->to($data['emailto'])
                ->replyTo($data['sender'], 'Administrador')
                ->subject($data['asunto']);

                echo 'The mail has been sent successfully';

            });

        }catch(\Swift_TransportException $e){
            $response = $e->getMessage() ;
            echo $response;
        }


        Mail::setSwiftMailer($backup);


        return Redirect::to('password')->with('success','Envio  Exitoso');
    }

}
