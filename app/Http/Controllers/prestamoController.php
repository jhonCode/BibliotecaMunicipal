<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\prestamo;
use Redirect;
/**
  *Clase encargada de realizar consultas a la tabla prestamo
  *
  */
class prestamoController extends Controller
{

    /**
    * funcion que se encarga de retornar los prestamos que no han finalizado.
    *
    *@return  vista la cual mostrara el resultado de la consulta a la bd.
    */  
    public function index()
    {
        
        $prestamo=DB::table('prestamo')
           ->join('users', 'users.id', '=', 'prestamo.idPersona')
           ->join('libro', 'libro.idLibro', '=', 'prestamo.idLibro')
           ->select('prestamo.idPrestamo','users.nombre','libro.titulo','prestamo.fechaInicio','prestamo.fechaFin','prestamo.devuelto','prestamo.dias')
            ->where('devuelto',0) 
            ->get();

         
          //  $now = new \DateTime();
           //$now = new \DateTime();
           //echo $now->format('Y-m-d');
              


             // var_dump($prestamo);
                   //$interval = $now->diff($prestamo->$fechaInicio);
                   //   echo $interval->format('%R%a');
                      
       return view('/php/listadoPrestamo',['prestamos' => $prestamo]);
    
    }

   /**
   * funcion que se encarga de finalizar un prestamo.
   *@param referencia al id del prestamo a modificar
   *@return  vista la cual mostrara el resultado de la consulta a la bd.
   */  
   public function edit($idPrestamo){
   	prestamo::editPrestamo($idPrestamo);
   	return redirect('/listadoPrestamos'); 
   }

   /**
   * funcion que se encarga de registar un prestamo.
   *@param datos equivalentes al prestamo
   *@return  vista la cual mostrara el resultado de la consulta a la bd.
   */  
   
   public function create(Request $request){

    prestamo::registrar($request);
    return Redirect::to('prestamo')->with('correcto','Registro Exitoso');

   }  


   





}
