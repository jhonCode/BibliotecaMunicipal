<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Input;
use Auth;
use View;
use Crypt;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }



    /**
    *devuelve datos showlogin blade
    *@return  string direccion login o al panel
    *
    */
     public function showLogin()
     {
            if (Auth::check())
                {
                    // Si está autenticado lo mandamos a la raíz donde estara el mensaje de bienvenida.
                    return Redirect::to('/home');
                }
                return View::make('/welcome');
     }

    /**
    *devuelve datos a la vista welcome blade
    *@return  string direccion  al panel  o manda mensaje de error
    *
    */
    public function postLogin(){

        $username=Input::get('lg_username');
        $password=Input::get('lg_password');
        $remember=Input::get('lg_remember');




        if(Auth::attempt(['username'=>$username,'password'=>$password],$remember))
            {
                return redirect()->intended('/home');
            }else{
               return Redirect::back()->with('error_message','datos Invalidos')->withInput();

           }

       }

    /**
    *cierra logout
    *@return  string direccion login
    *
    */
    public function logout(){
        Auth::logout();
        return Redirect::to('/welcome');
    } 



}
