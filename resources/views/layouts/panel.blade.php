<!DOCTYPE html>
<html lang="en" >

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>@yield('title')</title>
  
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
  <link href='http://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
  
  {!! Html::style('./bootstrap/css/bootstrap.min.css') !!}
  {!! Html::style('./bootstrap/css/login.css') !!}
  

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>


  <!--datepicker-->
  
  {!! Html::style('./bootstrap/css/css-date/bootstrap-datepicker3.css') !!}
  {!! Html::style('./bootstrap/css/css-date/bootstrap-datepicker.standalone.css') !!}

  {!! Html::script('./bootstrap/js/js-date/bootstrap-datepicker.js') !!}
  
  {!! Html::script('./bootstrap/js/locales-date/bootstrap-datepicker.es.min.js') !!}

</head>

<body>
  <nav class="navbar-toggleable-md  navbar-inverse bg-inverse">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" style="    margin-left: 14%;padding: 0px 17px;" href="#">Biblioteca Municipal
         {{ HTML::image('img/libros.png','Biblioteca',array('style' => 'position: absolute;top:7px;left:1px;')) }}
       </a>
     </div>

     <!-- Collect the nav links, forms, and other content for toggling -->
     <div class="collapse navbar-collapse pull-right" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
      <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Libro <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="/libro">RegistrarLibro</a></li>
            <li><a href="/listadolibros">EnlistarLibro</a></li>
            </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Usuario <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="/usuario">RegistrarUsuario</a></li>
            <li><a href="/listadoUsuarios">EnlistarUsuario</a></li>
            </ul>
        </li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Prestamos <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="/prestamo">RegistrarPrestamo</a></li>
            <li><a href="/listadoPrestamos">EnListarPrestamos</a></li>
            </ul>
        </li>
         <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Zonas <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="/zona">RegistrarZona</a></li>
            <li><a href="/listadoZonas">EnlistarZonas</a></li>
            </ul>
         </li>

         <li><a href="/salir">Log out</a></li>
      
      </ul>
      
      
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
@yield('contenido')



<!-- Include all compiled plugins (below), or include individual files as needed -->
{!! Html::script('./bootstrap/js/bootstrap.min.js') !!}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.min.js"></script>
{!! Html::script('./bootstrap/js/login.js') !!}

<script >



  $('.datepicker').datepicker({
    format: "dd-mm-yyyy",
    language: "es",
    autoclose: true
  });

  $('.dropdown-toggle').dropdown()




  $('.wrapper').css({'margin-top': $('.header').outerHeight()});
</script>
</body>

</html>
