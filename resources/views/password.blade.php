@extends('layouts.template')
 @Section('formulario') 

<!-- FORGOT PASSWORD FORM -->
<div class="text-center" style="padding:50px 0">
	<div class="logo">Olvido la contraseña</div>
	<!-- Main Form -->
	<div class="login-form-1">
		<form id="forgot-password-form" class="text-left" method="post" action="/correo" onsubmit="return pruebaemail()">
			  <input type="hidden" name="_token" value="{{ csrf_token() }}">
			  @if(Session::has('success'))
            <div class="alert alert-success" role= "alert">
              <strong>success:</strong>
                {!! session('success') !!}
                 </div>
                @endif
			<div class="main-login-form">
				<div class="login-group">
					<div class="form-group">
						<label for="fp_email" class="sr-only">Email address</label>
						<input type="text" class="form-control" id="fp_email" name="fp_email" placeholder="email address">
					</div>
				</div>
				<button type="submit" class="login-button"><i class="fa fa-chevron-right"></i></button>
			</div>
			<div class="etc-login-form">
				<p>ya tienes una cuenta? <a href="/">login </a></p>
			</div>
		</form>
	</div>
	<!-- end:Main Form -->
</div>

<script >
function pruebaemail (){
    re=/^([\da-z_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/

   var valor=document.getElementById('fp_email').value;
    if(!re.exec(valor))
    {
        alert('email no valido');
        return false;
    }else{
          return true;
    }
  

    }
</script>

@endsection