@extends('layouts.panel')

@section('title', 'Registro Usuario')

 @Section('contenido')
 <!-- REGISTRATION FORM -->
 <div class="text-center" style="padding:50px 0">
 	<div class="logo">Registro Usuario</div>
 	<!-- Main Form -->
 	<div class="login-form-1">
 		<form id="registeradmin-form" class="text-left" action="/registrarUsuario" method="post" onsubmit="return validar();">
 			<input type="hidden" name="_token" value="{{ csrf_token() }}">
 			@if(Session::has('success'))
    		<div class="alert alert-success" role= "alert">
      		  <strong>Successful:</strong>
            	{!! session('success') !!}
   				 </div>
				@endif
 			<div class="main-login-form">
 				<div class="login-group">

 					<div class="form-group">
 						<label for="reg_username" class="sr-only">identificacion</label>
 						<input type="text" class="form-control" id="reg_id" name="reg_id" placeholder="Id"onkeypress="return valida(event);" required>
 					</div>

 					<div class="form-group">
 						<label for="reg_nombre" class="sr-only">Nombre</label>
 						<input type="text" class="form-control" id="reg_nombre" name="reg_nombre" placeholder="Nombre" onkeypress="return soloLetras(event)" required>
 					</div>
 					<div class="form-group">
 						<label for="reg_apellido" class="sr-only">Apellido</label>
 						<input type="text" class="form-control" id="reg_apellido" name="reg_apellido" placeholder="Apellido" onkeypress="return soloLetras(event)" required>
 					</div>

 					<div class="form-group">
 						<label for="reg_edad" class="sr-only">Edad</label>
 						<input type="text" class="form-control" id="reg_edad" name="reg_edad" placeholder="Edad" onkeypress="return valida(event);"required>
 					</div>
 					<div class="form-group">
 						<label for="reg_direccion" class="sr-only">Direccion</label>
 						<input type="text" class="form-control" id="reg_direccion" name="reg_direccion" placeholder="Direcciòn" required>
 					</div>
 					<div class="form-group">
 						<label for="reg_email" class="sr-only">Email</label>
 						<input type="text" class="form-control" id="reg_email" name="reg_email" placeholder="Email" required>
 					</div>
 					<div class="form-group">
 						<label for="reg_telefono" class="sr-only">Telefono</label>
 						<input type="text" class="form-control" id="reg_telefono" name="reg_telefono" placeholder="Telefono" onkeypress="return valida(event);" required>
 					</div>

 					<div class="form-group">
 						<label for="rol">Rol</label>
 						<select  class="form-control" id="rol" name="rol" onchange="myFunction()">
 							<option  value="Administrador">Administrador</option>
 							<option value="Lector">Lector</option>
 						</select>
 					</div>

 					<div class="form-group" id="bloque">
 						<div class="form-group">
 							<label for="reg_username" class="sr-only">username</label>
 							<input type="text" class="form-control" id="reg_username" name="reg_username" placeholder="Username">
 						</div>	
 						<div class="form-group">
 							<label for="reg_password" class="sr-only">Password</label>
 							<input type="password" class="form-control" id="reg_password" name="reg_password" placeholder="password">
 						</div>
 						<div class="form-group">
 							<label for="reg_password_confirm" class="sr-only">Password Confirm</label>
 							<input type="password" class="form-control" id="reg_password_confirm" name="reg_password_confirm" placeholder="confirm password">
 						</div>
 					</div>
 				</div>
 				<button type="submit" class="login-button"><i class="fa fa-chevron-right"></i></button>
 			</div>
 		</form>
 	</div>
 	<!-- end:Main Form -->
 </div>


<script type="text/javascript">
	
	function myFunction() {
		var x = document.getElementById("rol").value;
		var b = document.getElementById('bloque');
		
		if(x==="Administrador"){
			b.setAttribute('style','display:block;');
		}else{
			b.setAttribute('style','display:none;');
			
		}
		

	}

function valida(e){
    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla==8){
        return true;
    }
        
    // Patron de entrada, en este caso solo acepta numeros
    patron =/[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}



function validar(){
    re=/^([\da-z_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/

   var valor1=document.getElementById('reg_email').value;
   

    if(!re.exec(valor1))
    {
        alert('correo no valido');
        return false;
    }
    
      



    }
   




function soloLetras(e){
  key = e.keyCode || e.which;
  tecla = String.fromCharCode(key).toLowerCase();
  letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
  especiales = "8-37-39-46";

       tecla_especial = false
       for(var i in especiales){
            if(key == especiales[i]){
                tecla_especial = true;
                break;
            }
        }

        if(letras.indexOf(tecla)==-1 && !tecla_especial){
            return false;
        }
    }



</script>
 @endsection