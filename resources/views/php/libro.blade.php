@extends('layouts.panel')

@section('title', 'Registro Libro')

@Section('contenido')


<!-- REGISTRATION FORM -->
<div class="text-center" style="padding:50px 0">
	<div class="logo">Registrar Libro</div>
	<!-- Main Form -->
	<div class="login-form-1">
		<form id="registerbook-form" class="text-left" action="/registrarLibro" method="post" enctype="multipart/form-data" >
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			
			<div class="main-login-form">
				<div class="login-group">
					<div class="form-group">
						<label for="reg_username" class="sr-only">ISBN</label>
						<input type="text" class="form-control" id="reg_isbn" name="reg_isbn" placeholder="ISBN" required>
					</div>
					<div class="form-group">
						<label for="reg_foto" class="sr-only">Imagen</label>
						<input type="file" class="form-control"  id="reg_foto" name="reg_foto" required>
					</div>
					<div class="form-group">
						<label for="reg_titulo" class="sr-only">Titulo</label>
						<input type="text" class="form-control" id="reg_titulo" name="reg_titulo" placeholder="Titulo" required>
					</div>
					
					<div class="form-group">
						<label for="reg_resumen" class="sr-only">Resumen</label>
						<textarea class="form-control" id="reg_resumen" name="reg_resumen" rows="3" placeholder="Resumen" maxlength="240" required></textarea>
					</div>
					<div class="form-group">
						<label for="reg_categoria" class="sr-only">Categoria</label>
						<input type="text" class="form-control" id="reg_categoria" name="reg_categoria" placeholder="Categoria" onkeypress="return valida(event);" required>
					</div>
					
					<div class="form-group">
						<label for="reg_ejemplar" class="sr-only"># Ejemplares</label>
						<input type="text" class="form-control" id="reg_ejemplar" name="reg_ejemplar" placeholder="# Ejemplares" onkeypress="return valida(event);" required>
					</div>
					<div class="form-group">
						<label for="reg_autor" class="sr-only">Autor</label>
						<input type="text" class="form-control" id="reg_autor" name="reg_autor" placeholder="Autor" required>
					</div>
					
					<div class="form-group">
						<label for="reg_zona">Zona</label>
					 <select class="form-control" id="reg_zona" name="reg_zona">
					 	 @foreach($zonas as $zona)
                            <option value="{{ $zona->idZona }}" {{ 'selected'  }}>{{ $zona->nombre }}
                            </option>
            	           @endforeach
					 </select>
						<!--
						<select  class="form-control" id="reg_zona" name="reg_zona">
							<option value="1">Ciencias Sociales</option>
							<option value="2">Matematicas</option>
							<option value="3">Fisica</option>
							<option value="4">Ciencias Natutales</option>
						</select>
					-->
					</div>
					
					
				</div>
				<button type="submit" class="login-button"><i class="fa fa-chevron-right"></i></button>
			</div>
			
		</form>
	</div>
	<!-- end:Main Form -->
</div>

<script type="text/javascript">

	function valida(e){
	    tecla = (document.all) ? e.keyCode : e.which;

	    //Tecla de retroceso para borrar, siempre la permite
	    if (tecla==8){
	        return true;
	    }
	        
	    // Patron de entrada, en este caso solo acepta numeros
	    patron =/[0-9]/;
	    tecla_final = String.fromCharCode(tecla);
	    return patron.test(tecla_final);
	}



	function validar(){
	    re=/^([\da-z_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/

	   var valor1=document.getElementById('reg_email').value;
	   

	    if(!re.exec(valor1))
	    {
	        alert('correo no valido');
	        return false;
	    }


	    }
	   




	function soloLetras(e){
	  key = e.keyCode || e.which;
	  tecla = String.fromCharCode(key).toLowerCase();
	  letras = " áéíóúabcdefghijklmnñopqrstuvwxyz";
	  especiales = "8-37-39-46";

	       tecla_especial = false
	       for(var i in especiales){
	            if(key == especiales[i]){
	                tecla_especial = true;
	                break;
	            }
	        }

	        if(letras.indexOf(tecla)==-1 && !tecla_especial){
	            return false;
	        }
	    }	

</script>
@endsection 
