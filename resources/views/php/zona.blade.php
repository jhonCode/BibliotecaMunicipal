@extends('layouts.panel')

@section('title', 'Registro Zona')

@Section('contenido')
<!-- REGISTRATION FORM -->
<div class="text-center" style="padding:50px 0">
	<div class="logo">Registrar Zona</div>
	<!-- Main Form -->
	<div class="login-form-1">
		<form id="registerzona-form" class="text-left" method="post" action="/registroZona">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="login-form-main-message"></div>
			<div class="main-login-form">
				<div class="login-group">
					<div class="form-group">
						<label for="reg_zona" class="sr-only">Nombre</label>
						<input type="text" class="form-control" id="reg_zona" name="reg_zona" placeholder="Zona">
					</div>		
				</div>
				<button type="submit" class="login-button"><i class="fa fa-chevron-right"></i></button>
			</div>
		</form>
	</div>
	<!-- end:Main Form -->
</div>

@endsection