@extends('layouts.panel')

@section('title', 'Listado Prestamo')

@Section('contenido')
<div class="container">           

  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header" style="color:white;text-align: center;">LISTADO DE PRESTAMOS</h1>
    </div>
  </div><!--/.row-->


  <table class="table table-hover" style="background-color:white;color:black; " >
   <thead>
    <tr>
     <th data-field="nombre" data-align="right">Usuario</th>
     <th data-field="titulo">Libro</th>
     <th data-field="fechainicio">Fecha Inicio</th>
     <th data-field="fechaentrega"> Fecha Entrega</th>
     <th data-field="dias">Dias Transcurridos</th>        
   </tr>
 </thead>
 <tbody>
   <tr>
    @foreach($prestamos as $prestamo)
    <td>{{$prestamo->nombre}}</td>
    <td>{{$prestamo->titulo}}</td>
    <td>{{$prestamo->fechaInicio}}</td>
    <td>{{$prestamo->fechaFin}}</td>    
    <td>{{$prestamo->dias}}</td>
    <td>
     <a class="btn btn-danger btn-xs" href="{{ route('prestamo/edit',['id' =>$prestamo->idPrestamo ] )}}" onclick="return confirm('¿Seguro desea terminar la devoluciòn del libro?')" >Finalizar</a> 
   </td>   
 </tr>
 @endforeach
</tbody>
</table>


</div><!--/.LISTADOS tabla row--> 


@endsection