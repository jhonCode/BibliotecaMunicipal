@extends('layouts.panel')

@section('title', 'Listado Usuarios')

@Section('contenido')
<div class="container">           

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header" style="color:white;text-align: center;">LISTADO DE USUARIOS</h1>
        </div>
    </div><!--/.row-->


    <table class="table table-hover" style="background-color:white;color:black; " >
       <thead>
          <tr>
             <th data-field="id" data-align="right">Identificacion</th>
             <th data-field="nombre">Nombre</th>
             <th data-field="apellido">Apellido</th>
             <th data-field="edad"> Edad</th>
             <th data-field="direccion">Direccion</th>
             <th data-field="email">Email</th>
             <th data-field="telefono"> Telefono</th>
             <th data-field="rol">Rol</th>
             <th data-field="username">Username</th>
             <th data-field="password">Password</th>
         </tr>
     </thead>
     <tbody>
       <tr>
          @foreach($usuarios as $usuario)
          <td>{{$usuario->documento}}</td>
          <td>{{$usuario->nombre}}</td>
          <td>{{$usuario->apellido}}</td>
          <td>{{$usuario->edad}}</td>    
          <td>{{$usuario->direccion}}</td>
          <td>{{$usuario->email}}</td>
          <td>{{$usuario->telefono}}</td>    
          <td>{{$usuario->rol}}</td>
          <td>{{$usuario->username}}</td>
          <td>{{$usuario->password2}}</td>
          <td>
             <a class="btn btn-primary btn-xs" href="{{ route('usuario/edit',['id' =>$usuario->id ] )}}" >Editar</a> 
             <a class="btn btn-danger btn-xs" href="{{ route('usuario/destroy',['id' =>$usuario->id] )}}" onclick="return confirm('¿Seguro desea eliminarlo?')" >Eliminar</a>
         </td>   
     </tr>
     @endforeach
 </tbody>
</table>


</div><!--/.LISTADOS tabla row--> 


@endsection