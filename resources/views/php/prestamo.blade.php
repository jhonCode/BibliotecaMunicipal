@extends('layouts.panel')

@section('title', 'Solicitar Prestamo')

 @Section('contenido')

<div class="text-center" style="padding:50px 0">
	<div class="logo">Buscar Usuario</div>
	<!-- Main Form -->
	<div class="login-form-1">
		<form id="bucarusuario" role="search" method="get" class="text-left" action="/buscarUsuario">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
			@if(Session::has('error'))
            <div class="alert alert-danger" role= "alert">
              <strong>Error:</strong>
                {!! session('error') !!}
                 </div>
                @endif
			<div class="main-login-form">
				<div class="login-group">
					<div class="form-group">
						<label for="usuario" class="sr-only">Nombre Usuario</label>
						<input type="text" class="form-control" id="usuario" name="usuario" placeholder="Usuario" required>
					</div>
				</div>
				<button type="submit" class="login-button"><i class="fa fa-chevron-right"></i></button>
			</div>
		</form>
	</div>
	<!-- end:Main Form -->
</div>
 @if(Session::has('success'))
          
              
<!-- buscar libro FORM -->
<div class="text-center" style="padding:0">
	<div class="logo">Buscar Libro</div>
	<!-- Main Form -->
	<div class="login-form-1">
		<form id="buscarlibro" class="text-left" role="search" method="get" class="text-left" action="/buscarLibro">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			@if(Session::has('error_libro'))
            <div class="alert alert-danger" role= "alert">
              <strong>Error:</strong>
                {!! session('error_libro') !!}
                 </div>
                @endif
			<div class="main-login-form">
				<div class="login-group">
					<div class="form-group">
						<label for="libro" class="sr-only">Buscar Libro</label>
						<input type="text" class="form-control" id="libro" name="libro" placeholder="Libro" required>
						<input type="text" class="form-control" id="user" name="user"  value="{!! session('success') !!}">
						
					</div>
				</div>
				<button type="submit" class="login-button"><i class="fa fa-chevron-right"></i></button>
			</div>

		</form>
	</div>
	<!-- end:Main Form -->
</div>

@endif

  @if(Session::has('usuario'))
<!-- Solicitud FORM -->
<div class="text-center" style="padding:0">
	<div class="logo">Generar Solicitud</div>
	<!-- Main Form -->
	<div class="login-form-1">
		<form id="buscarlibro" class="text-left" role="search" method="post" class="text-left" action="/generarSolicitud">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			@if(Session::has('error'))
            <div class="alert alert-danger" role= "alert">
              <strong>Error:</strong>
                {!! session('error') !!}
                 </div>
                @endif
                @if(Session::has('correcto'))
            <div class="alert alert-success" role= "alert">
              <strong>Error:</strong>
                {!! session('correcto') !!}
                 </div>
                @endif
			<div class="main-login-form">
				<div class="login-group">
					<div class="form-group">
						<div class="input-group">
								<input type="text" class="form-control datepicker" name="dateInicio" placeholder="Fecha Inicio" required>
						</div>
					</div>
					<div class="form-group">
						<div class="input-group">
								<input type="text" class="form-control datepicker" name="dateEntrega" placeholder="Fecha Entrega" required>
						</div>
					</div>
					<div class="form-group">
						<div class="input-group">
								<input type="text" class="form-control " name="libro" placeholder="libro" value="{!! session('success') !!}" readonly  required>
						</div>
					</div>
					<div class="form-group">
						<div class="input-group">
								<input type="text" class="form-control datepicker" name="usuario" placeholder="Usuario" value="{!! session('usuario') !!}" readonly required>
						</div>
					</div>

				</div>
				<button type="submit" class="login-button"><i class="fa fa-chevron-right"></i></button>
			</div>

		</form>
	</div>
	<!-- end:Main Form -->
</div>
   @endif


  


 @endsection