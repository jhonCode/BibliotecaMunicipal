
@extends('layouts.panel')

@section('title', 'Inicio')

@Section('contenido')



<!-- REGISTRATION FORM -->
<div class="text-center" style="padding:50px 0">
	<div class="logo">Actualizar Usuario</div>
	<!-- Main Form -->
	<div class="login-form-1">
		{!! Form::model($persona,['route' => 'usuario/update', 'class' => 'text-left','method' => 'post', 'novalidate' ]) !!}
		{!! Form::hidden('id', $persona->id) !!}

		<div class="login-form-main-message"></div>
		<div class="main-login-form">
			<div class="login-group">
				
				<div class="form-group">
					{!! Form::label('reg_id', 'Identificacion',['class' => 'sr-only']) !!}
					{!! Form::text('documento', null, ['class' => 'form-control' , 'required' => 'required','placeholder' =>'Identificacion','id'=>'reg_id','name'=>'reg_id']) !!}
				</div>
				
				<div class="form-group">
					{!! Form::label('reg_nombre', 'Nombre',['class' => 'sr-only']) !!}
					{!! Form::text('nombre', null, ['class' => 'form-control' , 'required' => 'required','placeholder' =>'Nombre','id'=>'reg_nombre','name'=>'reg_nombre']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('reg_apellido', 'Apellido',['class' => 'sr-only']) !!}
					{!! Form::text('apellido', null, ['class' => 'form-control' , 'required' => 'required','placeholder' =>'Nombre','id'=>'reg_apellido','name'=>'reg_apellido']) !!}
					
				</div>
				
				<div class="form-group">
					{!! Form::label('reg_edad', 'Edad',['class' => 'sr-only']) !!}
					{!! Form::text('edad', null, ['class' => 'form-control' , 'required' => 'required','placeholder' =>'Nombre','id'=>'reg_edad','name'=>'reg_edad']) !!}
					

				</div>
				<div class="form-group">

					{!! Form::label('reg_direccion', 'Direccion',['class' => 'sr-only']) !!}
					{!! Form::text('direccion', null, ['class' => 'form-control' , 'required' => 'required','placeholder' =>'Direccion','id'=>'reg_direccion','name'=>'reg_direccion']) !!}
					
				</div>
				<div class="form-group">
					
					{!! Form::label('reg_email', 'Email',['class' => 'sr-only']) !!}
					{!! Form::text('email', null, ['class' => 'form-control' , 'required' => 'required','placeholder' =>'Email','id'=>'reg_email','name'=>'reg_email']) !!}
					

				</div>
				<div class="form-group">
					{!! Form::label('reg_telefono', 'Telefono',['class' => 'sr-only']) !!}
					{!! Form::text('telefono', null, ['class' => 'form-control' , 'required' => 'required','placeholder' =>'Telefono','id'=>'reg_telefono','name'=>'reg_telefono']) !!}
				</div>

				<div class="form-group">
					{!! Form::label('Rol', 'Rol') !!}
					{{ Form::select('rol', ['Administrador'=> 'Administrador','Lector'=>'Lector'],null, ['id' => 'rol','class' => 'form-control','onchange'=>'myFunction()']) }}
					
				</div>

				<div class="form-group" id="bloque">
					<div class="form-group">
						{!! Form::label('reg_username', 'Username',['class' => 'sr-only']) !!}
						{!! Form::text('username', null, ['class' => 'form-control' , 'required' => 'required','placeholder' =>'Username','id'=>'reg_username','name'=>'reg_username']) !!}
					</div>	
					<div class="form-group">
						{!! Form::label('reg_password', 'Password',['class' => 'sr-only']) !!}
						{!! Form::text('password2', null, ['class' => 'form-control' , 'required' => 'required','placeholder' =>'Password','id'=>'reg_password','name'=>'reg_password']) !!}
						
					</div>
					<div class="form-group">
						{!! Form::label('reg_password_confirm', 'Password',['class' => 'sr-only']) !!}
						{!! Form::text('password2', null, ['class' => 'form-control' , 'required' => 'required','placeholder' =>'Password','id'=>'reg_password_confirm','name'=>'reg_password_confirm']) !!}
					</div>

				</div>

				
			</div>
			{!! Form::submit('Enviar', ['class' => 'btn btn-success ' ] ) !!}
		</button>
	</div>
	
</form>
</div>
<!-- end:Main Form -->
</div>

<script type="text/javascript">
	
	function myFunction() {
		var x = document.getElementById("rol").value;
		var b = document.getElementById('bloque');
		
		if(x==="Administrador"){
			b.setAttribute('style','display:block;');
		}else{
			b.setAttribute('style','display:none;');
			
		}
		

	}



</script>
@endsection 
