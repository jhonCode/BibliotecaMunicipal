@extends('layouts.panel')

@section('title', 'Listado Libros')

@Section('contenido')

  <div class="container"> 
    <div class="row">
      <div class="col-lg-12">
        <h1 class="page-header" style="color:white;text-align: center;">LISTADO DE LIBROS</h1>
      </div>
    </div><!--/.row-->


    <table class="table table-hover" style="background-color:white;color:black; " >
     <thead>
      <tr>
       <th data-field="id" data-align="right">Isbn</th>
       <th data-field="foto">Foto</th>
       <th data-field="titulo">Titulo</th>
       <th data-field="resumen"> Resumen</th>
       <th data-field="categoria">Categoria</th>
       <th data-field="ejemplares"> #Ejemplares</th>
       <th data-field="zon">Disponibles</th>
       <th data-field="autor"> Autor</th>
       <th data-field="zon">Zona</th>
     </tr>
   </thead>
   <tbody>
     <tr>
      @foreach($libros as $libro)
      <td>{{$libro->isbn}}</td>
      <td><img src="{{$libro->foto}}" alt="{{$libro->titulo}}" class="img-responsive" /></td>
      <td>{{$libro->titulo}}</td>
      <td>{{$libro->resumen}}</td>    
      <td>{{$libro->categoria}}</td>
      <td>{{$libro->ejemplares}}</td>
      <td>{{$libro->disponibles}}</td>    
      <td>{{$libro->autor}}</td>    
      <td>{{$libro->nombre}}</td>
      <td>
       <a class="btn btn-primary btn-xs" href="{{ route('libro/edit',['id' =>$libro->idLibro ] )}}" >Editar</a> 
       <a class="btn btn-danger btn-xs" href="{{ route('libro/destroy',['id' =>$libro->idLibro] )}}" onclick="return confirm('¿Seguro desea eliminarlo?')" >Eliminar</a>
     </td>   


     </tr>
   @endforeach
  </tbody>
  </table>


  </div><!--/.LISTADOS tabla row--> 


@endsection