@extends('layouts.panel')

@section('title', 'Listado Zonas')

@Section('contenido')

<div class="container"> 
  <div class="row">
    <div class="col-lg-12">
      <h1 class="page-header" style="color:white;text-align: center;">LISTADO DE ZONAS</h1>
    </div>
  </div><!--/.row-->


  <table class="table table-hover" style="background-color:white;color:black; " >
   <thead>
    <tr>
     <th data-field="id" data-align="right">Nombre</th>
     <th data-field="foto">Cantidad de Libros</th>
   </tr>
 </thead>
 <tbody>
   <tr>
    @foreach($zonas as $zona)
    <td>{{$zona->nombre}}</td>
    <td>{{$zona->total}}</td>
  </tr>
  @endforeach
</tbody>
</table>


</div><!--/.LISTADOS tabla row--> 


@endsection