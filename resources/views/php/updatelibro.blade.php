@extends('layouts.panel')

@section('title', 'Actualizar Libro')

@Section('contenido')


<!-- REGISTRATION FORM -->
<div class="text-center" style="padding:50px 0">
	<div class="logo">Actualizar Libro</div>
	<!-- Main Form -->
	<div class="login-form-1">
		{!! Form::model($libro,['route' => 'libro/update', 'class' => 'text-left','method' => 'post', 'novalidate','enctype'=>'multipart/form-data']) !!}
		{!! Form::hidden('id', $libro->idLibro) !!}

		<div class="login-form-main-message"></div>
		<div class="main-login-form">
			<div class="login-group">
				<div class="form-group">
					{!! Form::label('isbn', 'ISBN',['class' => 'sr-only']) !!}
					{!! Form::text('isbn', null, ['class' => 'form-control' , 'required' => 'required','placeholder' =>'ISBN','id'=>'reg_isbn','name'=>'reg_isbn']) !!}
				</div>	
				<div class="form-group">
					<label for="reg_password" class="sr-only">Imagen</label>
					<input type="file" class="form-control" accept="image/jpg, image/jpeg," id="reg_foto" name="reg_foto" hidden>
				</div>
				<div class="form-group">
					{!! Form::label('titulo', 'Titulo',['class' => 'sr-only']) !!}
					{!! Form::text('titulo', null, ['class' => 'form-control' , 'required' => 'required','placeholder' =>'Titulo','id'=>'reg_titulo','name'=>'reg_titulo']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('Resumen', 'Titulo',['class' => 'sr-only']) !!}
					{{ Form::textarea('resumen', null, ['class' => 'field','size' => '30x3']) }}

				</div>
				<div class="form-group">
					{!! Form::label('categoria', 'Titulo',['class' => 'sr-only']) !!}
					{!! Form::text('categoria', null, ['class' => 'form-control' , 'required' => 'required','placeholder' =>'Categoria','id'=>'reg_categoria','name'=>'reg_categoria']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('ejemplar', 'Ejemplar',['class' => 'sr-only']) !!}
					{!! Form::text('ejemplares', null, ['class' => 'form-control' , 'required' => 'required','placeholder' =>'Ejemplar','id'=>'reg_ejemplar','name'=>'reg_ejemplar']) !!}
				</div>

				<div class="form-group">
					{!! Form::label('ejemplar', 'Ejemplar',['class' => 'sr-only']) !!}
					{!! Form::text('disponibles', null, ['class' => 'form-control' , 'required' => 'required','placeholder' =>'Ejemplar','id'=>'reg_disponible','name'=>'reg_disponible']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('autor', 'Autor',['class' => 'sr-only']) !!}
					{!! Form::text('autor', null, ['class' => 'form-control' , 'required' => 'required','placeholder' =>'Autor','id'=>'reg_autor','name'=>'reg_autor']) !!}
				</div>
				<div class="form-group">
               	{{ Form::select('idZona',$zonas->pluck('nombre','idZona')->all(),null, ['id' => 'reg_zona','class' => 'form-control']) }}
		       
				</div>
			</div>
			{!! Form::submit('Enviar', ['class' => 'btn btn-success ' ] ) !!}
		</div>

		{!! Form::close() !!}
	</div>
	<!-- end:Main Form -->
</div>


@endsection