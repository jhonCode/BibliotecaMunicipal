<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Crypt;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $encrypted = Crypt::encrypt('admin');
                   //insertamos un administrador
    

    DB::table('users')->insert(array(
           'nombre'=> 'jaime',
           'apellido'=>'ramirez',
           'documento'=>'123123213',
           'edad'=>25,
           'direccion'=>'calarca Quindio',
            'email' => 'ramirescortes@hotmail.com',
            'telefono' =>'3126466',
            'rol'=> 'Administrador',
            'username'=>'admin',
            'password'=>\Hash::make('admin'),
            'password2'=>$encrypted
        )); 
    }
}
