<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
               $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('nombre',45);
            $table->string('apellido',45);
            $table->string('documento',45);
            $table->integer('edad');
            $table->string('direccion',45);
            $table->string('email',45);
            $table->string('telefono',45);
            $table->string('rol',45);
            $table->string('username',45)->nullable();
            $table->string('password',255)->nullable();
            $table->string('password2',255)->nullable();
            
            $table->rememberToken();
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
