<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLibroTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
            Schema::create('libro', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idLibro'); 
            $table->string('isbn');
            $table->string('foto',255);
            $table->string('titulo',255);
            $table->string('resumen',255);
            $table->integer('categoria');
            $table->string('autor',255);
            $table->integer('ejemplares');
            $table->integer('disponibles');
            $table->integer('idZona')->unsigned();

            $table->index('idZona','fk_Libro_Zona1_idx');    
            $table->foreign('idZona')
                ->references('idZona')->on('zona'); 


             $table->timestamps();
          
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('libro');
    }
}
