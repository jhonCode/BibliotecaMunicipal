<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrestamoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prestamo', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idPrestamo');
            $table->integer('idPersona')->unsigned();
            $table->integer('idLibro')->unsigned();
            $table->date('fechaInicio');
            $table->date('fechaFin');
            $table->boolean('devuelto');
            $table->integer('dias');
            
            
            $table->index('idPersona','fk_Prestamo_Persona1_idx');
            $table->index('idLibro','fk_Prestamo_Libro1_idx');
            
            $table->foreign('idPersona')
                   ->references('id')->on('users');
            
            $table->foreign('idLibro')
                   ->references('idLibro')->on('libro');
            


             $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prestamo');
    }
}
